CREATE view [dbo].[vw_transactions]

as

SELECT --top 1000

       CAST(ISNULL(Accounts.FullAccount, ' ') AS CHAR(55))                                                      AS glma_full_acct,

       CAST(ISNULL(Seg1.SegmentCode, ' ') AS CHAR(10))                                                          AS segment1,

       CAST(ISNULL(Seg1.Description, ' ') AS CHAR(30))                                                          AS segment1_description,

       Objs.ObjectCode                                                                                          AS object_code,

       CAST(ISNULL(Accounts.LongDescription, ' ') AS CHAR(30))                                                  AS account_description,

       CAST(ISNULL(Seg2.SegmentCode, ' ') AS CHAR(10))                                                          AS segment2,

       CAST(ISNULL(Seg2.Description, ' ') AS CHAR(30))                                                          AS segment2_description,

       CAST(ISNULL(Seg3.SegmentCode, ' ') AS CHAR(10))                                                          AS segment3,

       CAST(ISNULL(Seg3.Description, ' ') AS CHAR(30))                                                          AS segment3_description,

       CAST(ISNULL(Seg4.SegmentCode, ' ') AS CHAR(10))                                                          AS segment4,

       CAST(ISNULL(Seg4.Description, ' ') AS CHAR(30))                                                          AS segment4_description,

       CAST(ISNULL(Seg5.SegmentCode, ' ') AS CHAR(10))                                                          AS segment5,

       CAST(ISNULL(Seg5.Description, ' ') AS CHAR(30))                                                          AS segment5_description,

       CAST(ISNULL(Seg6.SegmentCode, ' ') AS CHAR(10))                                                          AS segment6,

       CAST(ISNULL(Seg6.Description, ' ') AS CHAR(30))                                                          AS segment6_description,

       CAST(ISNULL(Seg7.SegmentCode, ' ') AS CHAR(10))                                                          AS segment7,

       CAST(ISNULL(Seg7.Description, ' ') AS CHAR(30))                                                          AS segment7_description,

       CAST(ISNULL(Seg8.SegmentCode, ' ') AS CHAR(10))                                                          AS segment8,

       CAST(ISNULL(Seg8.Description, ' ') AS CHAR(30))                                                          AS segment8_description,

       CAST(ISNULL(Objs.CharacterCode, ' ') AS CHAR(2))                                                         AS character_code,

       CAST(ISNULL(cc.glch_desc, ' ') AS CHAR(50))                                                              AS character_code_description,

       CAST(ISNULL(Projs.ProjectCode, ' ') AS CHAR(5))                                                          AS project_code,

       CAST(ISNULL(RefOrgs.OrganizationCode, ' ') AS Char(8))                                                   AS reference_organization_code,

       Orgs.OrganizationCode                                                                                    AS organization_code,

       Accounts.AccountType                                                                                     AS account_type,

       CAST(ISNULL(Accounts.BalanceType, ' ')AS CHAR(1))                                                        AS balance_type,

       Accounts.Status                                                                                          AS status,

       CAST((CASE WHEN AutoEncumber = 'True' THEN 'Y' ELSE 'N' END) AS CHAR(1))                                 AS encumb_budget,

       CAST((CASE WHEN IsBudgetary = 'True' THEN 'Y' ELSE 'N' END) AS CHAR(1))                                  AS budgetary,

       CAST(ISNULL(Accounts.NormalBalanceType, ' ') AS CHAR(1))     AS normal_balance_sign,

       jrnl.Year * 100 + jrnl.Period                                    AS journal_year_period,

       jrnl.Year                                                        AS journal_year,

       jrnl.Period                                                      AS journal_period,

       jrnl.JournalNumber                                               AS journal_number,

       jrnl_line.Sequence                                               AS journal_sequence_no,

       jrnl_line.Source                                                 AS journal_ledger_type,

       CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') AND ISNUMERIC(jrnl_line.Reference1) = 1  THEN

              CASE WHEN  LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL') AND  ISNULL(jrnl_line.Reference3, '') != '' THEN ISNULL(jrnl_line.Reference1, ' ')


                     WHEN  LEFT(ISNULL(jrnl_line.Source, ' '),3) = 'POE' THEN ISNULL(jrnl_line.Reference1, ' ')


             ELSE ''

                END
        ELSE
           ''
       END    AS vendor_id,

	  CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN

	       CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN

                 (SELECT top 1 vendor_remit.Name

				   FROM mu_live.dbo.apinvoih AS invoice_vendor

					JOIN mu_live.dbo.Vendors as vendor on vendor.VendorNumber = invoice_vendor.apih_vendor

					JOIN mu_live.dbo.VendorRemittances as vendor_remit on vendor_remit.VendorId = vendor.Id

					                                                      and vendor_remit.RemittanceNumber = invoice_vendor.apih_remit_no

				   WHERE invoice_vendor.apih_vendor = jrnl_line.Reference1

					AND invoice_vendor.apih_doc = jrnl_line.Reference3)

                ELSE ' '
            END

	  ELSE ''

	 END AS vendor_name,

     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN

            ISNULL(jrnl_line.Reference2, ' ')

      ELSE  ''

     END   AS po_no,

     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN

          CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN

                 (SELECT invoice.apih_inv_num FROM mu_live.dbo.apinvoih AS invoice WHERE invoice.apih_vendor = jrnl_line.Reference1 AND invoice.apih_doc = jrnl_line.Reference3)

                ELSE

         ISNULL(jrnl_line.Reference3, ' ')
            END

      ELSE ''
     END    AS invoice_no,

     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN

          CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN

                 (SELECT
				        case when invoice_check.apih_check_no != 0 then convert(varchar(8),invoice_check.apih_check_no)

						else ''
						end

						FROM mu_live.dbo.apinvoih AS invoice_check WHERE invoice_check.apih_vendor = jrnl_line.Reference1 AND invoice_check.apih_doc = jrnl_line.Reference3)

                ELSE ''
            END
      ELSE ''
     END    AS check_no,

     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') OR ISNUMERIC(jrnl_line.Reference1) = 0 THEN

            ISNULL(jrnl_line.Reference1, ' ')

      ELSE ''

     END    AS reference_1,

     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') THEN

            ISNULL(jrnl_line.Reference2, ' ')

      ELSE ''
     END    AS reference_2,

     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') THEN

            ISNULL(jrnl_line.Reference3, ' ')

      ELSE ''
     END    AS reference_3,

       ISNULL(jrnl_line.Reference4, ' ')                   AS journal_description,

       ISNULL(jrnl_line.Source, ' ')                       AS journal_source,

       ISNULL(jrnl_line.TransactionType, ' ')              AS transaction_type,

       ISNULL(jrnl_line.BudgetType, ' ')                   AS budget_type,

       ISNULL(jrnl_line.Comment, ' ')                      AS journal_comment,

       CASE WHEN jrnl_line.OverBudget = 'True' THEN 'Y' ELSE 'N' END    AS over_budget,

       ISNULL(jrnl_line.AutoManual, ' ')                   AS auto_or_manual,

       jrnl.Year                                              AS transact_year,

       jrnl.Period                                 AS transact_period,

       jrnl_line.EffectiveDate  AS effective_date,

       jrnl.EntryDate           AS entry_date,

       jrnl.PostedDate           AS posted_date,

     ISNULL(jrnl_line.Gross, 0.00)                       AS gross_amount,

     ISNULL(jrnl_line.Debit, 0.00)                       AS debit_amount,

     ISNULL(jrnl_line.Credit, 0.00)                       AS credit_amount,

     jrnl.Phase                                          AS journal_phase,

	   Seg1.Description,

     projs.Description as project_description,

     projs.Title as project_title

   FROM mu_live.dbo.Journals AS jrnl

  --JOIN mu_live.dbo.JournalLineItems jrnl_line on jrnl_line.JournalID = jrnl.ID --and jrnl_line.Year = jrnl.Year and jrnl_line.Period = jrnl.Period

   JOIN mu_live.dbo.JournalLineItems jrnl_line on jrnl_line.JournalNumber = jrnl.JournalNumber and jrnl_line.Year = jrnl.Year and jrnl_line.Period = jrnl.Period

       INNER JOIN mu_live.dbo.Accounts AS Accounts ON Accounts.Id = jrnl_line.AccountId

       INNER JOIN mu_live.dbo.Organizations AS Orgs ON Accounts.OrganizationId = Orgs.Id

       INNER JOIN mu_live.dbo.GLObjects AS Objs ON Accounts.ObjectId = Objs.Id

       LEFT JOIN mu_live.dbo.Projects AS Projs ON Accounts.ProjectId = Projs.Id

       --LEFT JOIN mu_live.dbo.GLObjects AS RefObj

       --  ON RefObj.Id = Accounts.ReferenceObjId

       LEFT JOIN mu_live.dbo.Organizations AS RefOrgs

          ON RefOrgs.Id = Accounts.ReferenceOrgId

       LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg1

                 INNER JOIN mu_live.dbo.Segments AS Seg1 ON OrgSeg1.SegmentId = Seg1.Id AND Seg1.Type  = 1

         ON Orgs.Id = OrgSeg1.OrganizationId

       LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg2

                 INNER JOIN mu_live.dbo.Segments AS Seg2 ON OrgSeg2.SegmentId = Seg2.Id AND Seg2.Type  = 2

         ON Orgs.Id = OrgSeg2.OrganizationId

       LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg3

                 INNER JOIN mu_live.dbo.Segments AS Seg3 ON OrgSeg3.SegmentId = Seg3.Id AND Seg3.Type  = 3

         ON Orgs.Id = OrgSeg3.OrganizationId

       LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg4

                 INNER JOIN mu_live.dbo.Segments AS Seg4 ON OrgSeg4.SegmentId = Seg4.Id AND Seg4.Type  = 4

         ON Orgs.Id = OrgSeg4.OrganizationId

       LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg5

                 INNER JOIN mu_live.dbo.Segments AS Seg5 ON OrgSeg5.SegmentId = Seg5.Id AND Seg5.Type  = 5

         ON Orgs.Id = OrgSeg5.OrganizationId

       LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg6

                 INNER JOIN mu_live.dbo.Segments AS Seg6 ON OrgSeg6.SegmentId = Seg6.Id AND Seg6.Type  = 6

         ON Orgs.Id = OrgSeg6.OrganizationId

       LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg7

                 INNER JOIN mu_live.dbo.Segments AS Seg7 ON OrgSeg7.SegmentId = Seg7.Id AND Seg7.Type  = 7

         ON Orgs.Id = OrgSeg7.OrganizationId

       LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg8

                 INNER JOIN mu_live.dbo.Segments AS Seg8 ON OrgSeg8.SegmentId = Seg8.Id AND Seg8.Type  = 8

         ON Orgs.Id = OrgSeg8.OrganizationId

       LEFT JOIN mu_live.dbo.glcharcd_table AS cc ON cc.glch_code = Objs.CharacterCode


WHERE jrnl.Year >= 2012
