alter VIEW [dbo].[munis_transactions]

AS



SELECT Acct.FullAccount                                                 AS account_number,

       Acct.AccountType                                                 AS account_type,

       case when isnull(Acct.BalanceType, '') = '' then Acct.AccountType

            else Acct.BalanceType

       end                                                              As balance_type,

       ISNULL(Seg1.SegmentCode, '')                                     AS fund,

	   ISNULL(Seg1.Description, '')                                     as fund_name,



       ISNULL(Seg2.SegmentCode, '')                                     AS segment2,

       ISNULL(Seg2.Description, '')                                     AS segment2_name,

       ISNULL(Seg3.SegmentCode, '')                                     AS segment3,

       ISNULL(Seg3.Description, '')                                     AS segment3_name,

       ISNULL(Seg4.SegmentCode, '')                                     AS segment4,

       ISNULL(Seg4.Description, '')                                     AS segment4_name,

       ISNULL(Seg5.SegmentCode, '')                                     AS segment5,

       ISNULL(Seg5.Description, '')                                     AS segment5_name,

       ISNULL(Seg6.SegmentCode, '')                                     AS segment6,

       ISNULL(Seg6.Description, '')                                     AS segment6_name,

       ISNULL(Seg7.SegmentCode, '')                                     AS segment7,

       ISNULL(Seg7.Description, '')                                     AS segment7_name,

       ISNULL(Seg8.SegmentCode, '')                                     AS segment8,

       ISNULL(Seg8.Description, '')                                     AS segment8_name,

       ISNULL(Obj.ObjectCode, '')                                       AS object_code,

       ISNULL(Obj.CharacterCode, ' ')                                  AS character_code,

       --ISNULL(cc.glch_desc, ' ')                                        AS character_code_description,

       ISNULL(Proj.ProjectCode, ' ')                                   AS project_code,

       --ISNULL(RefOrgs.OrganizationCode, ' ')                            AS reference_organization_code,

       Org.OrganizationCode                                            AS organization_code,

       ISNULL(Obj.LongDescription, '')                                  AS object_description,

       jrnl.Year * 100 + jrnl.Period                                    AS journal_year_period,

       jrnl.Year                                                        AS journal_year,

       jrnl.Period                                                      AS journal_period,

       jrnl.JournalNumber                                               AS journal_number,

       jrnl_line.Sequence                                               AS journal_sequence_no,

       jrnl_line.Source                                                 AS journal_ledger_type,

     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') AND ISNUMERIC(jrnl_line.Reference1) = 1  THEN

              CASE WHEN  LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL') AND  ISNULL(jrnl_line.Reference3, '') != '' THEN ISNULL(jrnl_line.Reference1, ' ')

                     WHEN  LEFT(ISNULL(jrnl_line.Source, ' '),3) = 'POE' THEN ISNULL(jrnl_line.Reference1, ' ')

             ELSE ''

                END

      ELSE

      ''

     END    AS vendor_id,

	 CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN

	        CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN

                 (SELECT top 1 vendor_remit.Name

				    FROM muntownprod.dbo.apinvoih AS invoice_vendor

					JOIN muntownprod.dbo.Vendors as vendor on vendor.Id = invoice_vendor.apih_vendor

					JOIN muntownprod.dbo.VendorRemittances as vendor_remit on vendor_remit.VendorId = vendor.Id

					                                                       and vendor_remit.RemittanceNumber = invoice_vendor.apih_remit_no

				    WHERE invoice_vendor.apih_vendor = jrnl_line.Reference1

					AND invoice_vendor.apih_doc = jrnl_line.Reference3)

                ELSE

				 ' '

            END

			ELSE

			  ''

	  END AS vendor_name,



     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN

            ISNULL(jrnl_line.Reference2, ' ')

      ELSE

      ''

     END    AS po_no,



     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN

          CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN

                 (SELECT invoice.apih_inv_num FROM muntownprod.dbo.apinvoih AS invoice WHERE invoice.apih_vendor = jrnl_line.Reference1 AND invoice.apih_doc = jrnl_line.Reference3)

                ELSE

         ISNULL(jrnl_line.Reference3, ' ')

            END

      ELSE

      ''

     END    AS invoice_no,



     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN

          CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN

                 (SELECT

				         case when invoice_check.apih_check_no != 0 then convert(varchar(8),invoice_check.apih_check_no)

						 else

						   ''

						 end

						 FROM muntownprod.dbo.apinvoih AS invoice_check WHERE invoice_check.apih_vendor = jrnl_line.Reference1 AND invoice_check.apih_doc = jrnl_line.Reference3)

                ELSE

         ''

            END

      ELSE

      ''

     END    AS check_no,





     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') OR ISNUMERIC(jrnl_line.Reference1) = 0 THEN

            ISNULL(jrnl_line.Reference1, ' ')

      ELSE

      ''

     END    AS reference_1,



     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') THEN

            ISNULL(jrnl_line.Reference2, ' ')

      ELSE

      ''

     END    AS reference_2,



     CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') THEN

            ISNULL(jrnl_line.Reference3, ' ')

      ELSE

      ''

     END    AS reference_3,



       ISNULL(jrnl_line.Reference4, ' ')                   AS journal_description,

       ISNULL(jrnl_line.Source, ' ')                       AS journal_source,

       ISNULL(jrnl_line.TransactionType, ' ')              AS transaction_type,

       ISNULL(jrnl_line.BudgetType, ' ')                   AS budget_type,

       ISNULL(jrnl_line.Comment, ' ')                      AS journal_comment,

       CASE WHEN jrnl_line.OverBudget = 'True' THEN 'Y' ELSE 'N' END    AS over_budget,

       ISNULL(jrnl_line.AutoManual, ' ')                   AS auto_or_manual,



       jrnl.Year                                              AS transact_year,

       jrnl.Period                                            AS transact_period,

       jrnl_line.EffectiveDate  AS effective_date,

       jrnl.EntryDate           AS entry_date,



     ISNULL(jrnl_line.Gross, 0.00)                       AS gross_amount,

     ISNULL(jrnl_line.Debit, 0.00)                       AS debit_amount,

     ISNULL(jrnl_line.Credit, 0.00)                       AS credit_amount,

     jrnl.Phase                                          AS journal_phase,

	 Seg1.Description



   FROM muntownprod.dbo.Journals AS jrnl

       INNER JOIN muntownprod.dbo.JournalLineItems AS jrnl_line ON jrnl_line.JournalNumber  = jrnl.Id

       INNER JOIN muntownprod.dbo.Accounts AS Acct ON Acct.Id = jrnl_line.AccountId

       INNER JOIN muntownprod.dbo.Organizations AS Org ON Acct.OrganizationId = Org.Id

       INNER JOIN muntownprod.dbo.GLObjects AS Obj ON Acct.ObjectId = Obj.Id

       LEFT JOIN muntownprod.dbo.Projects AS Proj ON Acct.ProjectId = Proj.Id

     --

       LEFT JOIN muntownprod.dbo.OrganizationSegments AS OrgSeg1

                 INNER JOIN muntownprod.dbo.Segments AS Seg1 ON OrgSeg1.SegmentId = Seg1.Id AND Seg1.Type  = 1

         ON Org.Id = OrgSeg1.OrganizationId

       LEFT JOIN muntownprod.dbo.OrganizationSegments AS OrgSeg2

  INNER JOIN muntownprod.dbo.Segments AS Seg2 ON OrgSeg2.SegmentId = Seg2.Id AND Seg2.Type  = 2

         ON Org.Id = OrgSeg2.OrganizationId

       LEFT JOIN muntownprod.dbo.OrganizationSegments AS OrgSeg3

                 INNER JOIN muntownprod.dbo.Segments AS Seg3 ON OrgSeg3.SegmentId = Seg3.Id AND Seg3.Type  = 3

         ON Org.Id = OrgSeg3.OrganizationId

       LEFT JOIN muntownprod.dbo.OrganizationSegments AS OrgSeg4

                 INNER JOIN muntownprod.dbo.Segments AS Seg4 ON OrgSeg4.SegmentId = Seg4.Id AND Seg4.Type  = 4

         ON Org.Id = OrgSeg4.OrganizationId

       LEFT JOIN muntownprod.dbo.OrganizationSegments AS OrgSeg5

                 INNER JOIN muntownprod.dbo.Segments AS Seg5 ON OrgSeg5.SegmentId = Seg5.Id AND Seg5.Type  = 5

         ON Org.Id = OrgSeg5.OrganizationId

       LEFT JOIN muntownprod.dbo.OrganizationSegments AS OrgSeg6

                 INNER JOIN muntownprod.dbo.Segments AS Seg6 ON OrgSeg6.SegmentId = Seg6.Id AND Seg6.Type  = 6

         ON Org.Id = OrgSeg6.OrganizationId

       LEFT JOIN muntownprod.dbo.OrganizationSegments AS OrgSeg7

                 INNER JOIN muntownprod.dbo.Segments AS Seg7 ON OrgSeg7.SegmentId = Seg7.Id AND Seg7.Type  = 7

         ON Org.Id = OrgSeg7.OrganizationId

       LEFT JOIN muntownprod.dbo.OrganizationSegments AS OrgSeg8

                 INNER JOIN muntownprod.dbo.Segments AS Seg8 ON OrgSeg8.SegmentId = Seg8.Id AND Seg8.Type  = 8

         ON Org.Id = OrgSeg8.OrganizationId
