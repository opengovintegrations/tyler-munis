



CREATE VIEW [dbo].[chart_of_accounts]

AS



SELECT

CAST(ISNULL(Accounts.FullAccount, ' ') AS CHAR(55))                                                      AS glma_full_acct,

CAST(ISNULL(Seg1.SegmentCode, ' ') AS CHAR(10))                                                          AS segment1,

CAST(ISNULL(Seg1.Description, ' ') AS CHAR(30))                                                          AS segment1_description,

Objs.ObjectCode                                                                                          AS object_code,

CAST(ISNULL(Accounts.LongDescription, ' ') AS CHAR(30))                                                  AS [description],

CAST(ISNULL(Seg2.SegmentCode, ' ') AS CHAR(10))                                                          AS segment2,

CAST(ISNULL(Seg2.Description, ' ') AS CHAR(30))                                                          AS segment2_description,

CAST(ISNULL(Seg3.SegmentCode, ' ') AS CHAR(10))                                                          AS segment3,

CAST(ISNULL(Seg3.Description, ' ') AS CHAR(30))                                                          AS segment3_description,

CAST(ISNULL(Seg4.SegmentCode, ' ') AS CHAR(10))                                                          AS segment4,

CAST(ISNULL(Seg4.Description, ' ') AS CHAR(30))                                                          AS segment4_description,

CAST(ISNULL(Seg5.SegmentCode, ' ') AS CHAR(10))                                                          AS segment5,

CAST(ISNULL(Seg5.Description, ' ') AS CHAR(30))                                                          AS segment5_description,

CAST(ISNULL(Seg6.SegmentCode, ' ') AS CHAR(10))                                                          AS segment6,

CAST(ISNULL(Seg6.Description, ' ') AS CHAR(30))                                                          AS segment6_description,

CAST(ISNULL(Seg7.SegmentCode, ' ') AS CHAR(10))                                                          AS segment7,

CAST(ISNULL(Seg7.Description, ' ') AS CHAR(30))                                                          AS segment7_description,

CAST(ISNULL(Seg8.SegmentCode, ' ') AS CHAR(10))                                                          AS segment8,

CAST(ISNULL(Seg8.Description, ' ') AS CHAR(30))                                                          AS segment8_description,

CAST(ISNULL(Objs.CharacterCode, ' ') AS CHAR(2))                                                         AS character_code,

CAST(ISNULL(cc.glch_desc, ' ') AS CHAR(50))                                                              AS character_code_description,

CAST(ISNULL(Projs.ProjectCode, ' ') AS CHAR(5))                                                          AS project_code,

CAST(ISNULL(RefOrgs.OrganizationCode, ' ') AS CHAR(8))                                                   AS reference_organization_code,

Orgs.OrganizationCode                                                                                    AS organization_code,

Accounts.AccountType                                                                                     AS account_type,

CAST(ISNULL(Accounts.BalanceType, ' ')AS CHAR(1))                                                        AS balance_type,

Accounts.Status                                                                                          AS status,

CAST((CASE WHEN AutoEncumber = 'True' THEN 'Y' ELSE 'N' END) AS CHAR(1))                                 AS encumb_budget,

CAST((CASE WHEN IsBudgetary = 'True' THEN 'Y' ELSE 'N' END) AS CHAR(1))                                  AS budgetary,

CAST(ISNULL(Accounts.NormalBalanceType, ' ') AS CHAR(1))                                                 AS normal_balance_sign

      FROM mu_live.dbo.Accounts

INNER JOIN mu_live.dbo.GLObjects AS Objs

        ON Objs.Id = mu_live.dbo.Accounts.ObjectId

INNER JOIN mu_live.dbo.Organizations AS Orgs

        ON Orgs.Id = mu_live.dbo.Accounts.OrganizationId

 LEFT JOIN mu_live.dbo.Projects AS Projs

        ON Projs.Id = mu_live.dbo.Accounts.ProjectId

 LEFT JOIN  mu_live.dbo.OrganizationSegments AS OrgSeg1

INNER JOIN  mu_live.dbo.Segments AS Seg1

        ON OrgSeg1.SegmentId = Seg1.Id

       AND Seg1.Type = 1

        ON Orgs.Id = OrgSeg1.OrganizationId

 LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg2

INNER JOIN mu_live.dbo.Segments AS Seg2

        ON OrgSeg2.SegmentId = Seg2.Id

       AND Seg2.Type = 2 ON Orgs.Id = OrgSeg2.OrganizationId

 LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg3

INNER JOIN mu_live.dbo.Segments AS Seg3

        ON OrgSeg3.SegmentId = Seg3.Id

       AND Seg3.Type = 3

        ON Orgs.Id = OrgSeg3.OrganizationId

 LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg4

INNER JOIN mu_live.dbo.Segments AS Seg4

        ON OrgSeg4.SegmentId = Seg4.Id

       AND Seg4.Type = 4

        ON Orgs.Id = OrgSeg4.OrganizationId

 LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg5

INNER JOIN mu_live.dbo.Segments AS Seg5

        ON OrgSeg5.SegmentId = Seg5.Id

       AND Seg5.Type = 5

        ON Orgs.Id = OrgSeg5.OrganizationId

 LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg6

INNER JOIN mu_live.dbo.Segments AS Seg6

        ON OrgSeg6.SegmentId = Seg6.Id

       AND Seg6.Type = 6

        ON Orgs.Id = OrgSeg6.OrganizationId

 LEFT JOIN mu_live.dbo.OrganizationSegments AS OrgSeg7

INNER JOIN mu_live.dbo.Segments AS Seg7

        ON OrgSeg7.SegmentId = Seg7.Id

       AND Seg7.Type = 7

        ON Orgs.Id = OrgSeg7.OrganizationId

 LEFT JOIN mu_live.dbo.OrganizationSegments

        AS OrgSeg8

INNER JOIN mu_live.dbo.Segments AS Seg8

        ON OrgSeg8.SegmentId = Seg8.Id

       AND Seg8.Type = 8

        ON Orgs.Id = OrgSeg8.OrganizationId

 LEFT JOIN mu_live.dbo.Organizations AS RefOrgs

        ON RefOrgs.Id = Accounts.ReferenceOrgId

 LEFT JOIN mu_live.dbo.GLObjects AS RefObj

        ON RefObj.Id = Accounts.ReferenceObjId

 LEFT JOIN mu_live.dbo.Projects AS RefProj

        ON RefProj.Id = Accounts.ReferenceProjId

 LEFT JOIN mu_live.dbo.Grants

        ON Grants.Id = Accounts.GrantId

LEFT JOIN mu_live.dbo.glcharcd_table AS cc ON cc.glch_code = Objs.CharacterCode
