ALTER VIEW [dbo].[vw_transactions]
AS

SELECT Acct.FullAccount                                                 AS account_number,
       Acct.AccountType                                                 AS account_type,
       ISNULL(Seg1.SegmentCode, ' ')                                    AS fund,
       ---Org.OrganizationCode                                           as organizational_code,
	   Seg1.description                                                 AS fund_name,
       --ISNULL(Org.Segrem, ' ')                                        as a_remaining_orgseg,
	   seg2.SegmentCode                                                 AS department,
	   seg2.Description                                                 AS department_name,
       Obj.ObjectCode                                                   AS object_code,
	   Obj.LongDescription                                              AS object_description,
     CAST(ISNULL(Acct.BalanceType, ' ')AS CHAR(1))                                                        AS balance_type,
     CAST(ISNULL(Seg2.SegmentCode, ' ') AS CHAR(10))                                                          AS segment2,

     CAST(ISNULL(Seg2.Description, ' ') AS CHAR(30))                                                          AS segment2_description,

     CAST(ISNULL(Seg3.SegmentCode, ' ') AS CHAR(10))                                                          AS segment3,

     CAST(ISNULL(Seg3.Description, ' ') AS CHAR(30))                                                          AS segment3_description,

     CAST(ISNULL(Seg4.SegmentCode, ' ') AS CHAR(10))                                                          AS segment4,

     CAST(ISNULL(Seg4.Description, ' ') AS CHAR(30))                                                          AS segment4_description,

     CAST(ISNULL(Seg5.SegmentCode, ' ') AS CHAR(10))                                                          AS segment5,

     CAST(ISNULL(Seg5.Description, ' ') AS CHAR(30))                                                          AS segment5_description,

     CAST(ISNULL(Seg6.SegmentCode, ' ') AS CHAR(10))                                                          AS segment6,

     CAST(ISNULL(Seg6.Description, ' ') AS CHAR(30))                                                          AS segment6_description,

     CAST(ISNULL(Seg7.SegmentCode, ' ') AS CHAR(10))                                                          AS segment7,

     CAST(ISNULL(Seg7.Description, ' ') AS CHAR(30))                                                          AS segment7_description,

     CAST(ISNULL(Seg8.SegmentCode, ' ') AS CHAR(10))                                                          AS segment8,

     CAST(ISNULL(Seg8.Description, ' ') AS CHAR(30))                                                          AS segment8_description,
       --ISNULL(Proj.ProjectCode, ' ')                                  as project,
       jrnl.Year * 100 + jrnl.Period                  AS journal_year_period,
	   jrnl.Year                                              AS journal_year,
	   jrnl.Period                                            AS journal_period,
       jrnl.JournalNumber                                     AS journal_number,
       jrnl_line.Sequence                                  AS journal_sequence_no,
	   jrnl_line.source                                    AS journal_ledger_type,
       --ISNULL(jrnl_line.EntryClerk, ' ')                   as h_clerk,
	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') AND ISNUMERIC(jrnl_line.Reference1) = 1  THEN
	            CASE WHEN  LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL') AND  ISNULL(jrnl_line.Reference3, '') != '' THEN ISNULL(jrnl_line.Reference1, ' ')
                     WHEN  LEFT(ISNULL(jrnl_line.Source, ' '),3) = 'POE' THEN ISNULL(jrnl_line.Reference1, ' ')
				     ELSE ''
                END
			ELSE
			''
	   END 		AS vendor_id,
	   -- ISNULL(jrnl_line.Reference1, ' '),
	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') AND ISNUMERIC(jrnl_line.Reference1) = 1  THEN
	            CASE WHEN  LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL') AND  ISNULL(jrnl_line.Reference3, '') != ''
				           THEN ISNULL((SELECT TOP 1 vendor_remit.Name FROM [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.VendorRemittances vendor_remit WHERE vendor_remit.VendorId = jrnl_line.Reference1 AND vendor_remit.IsActive  = 1),'')
                     WHEN  LEFT(ISNULL(jrnl_line.Source, ' '),3) = 'POE'
					       THEN ISNULL((SELECT TOP 1 vendor_remit.Name FROM [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.VendorRemittances vendor_remit WHERE vendor_remit.VendorId = jrnl_line.Reference1 AND vendor_remit.IsActive  = 1),'')
				     ELSE ''
                END
			ELSE
			''
	   END 		AS vendor_name,


	   --CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') AND ISNUMERIC(jrnl_line.Reference1) = 1  AND ISNULL(jrnl_line.Reference3, '') != '' THEN
       --        ISNULL((SELECT TOP 1 dbo.VendorRemittances.Name FROM dbo.VendorRemittances WHERE dbo.VendorRemittances.VendorId = jrnl_line.Reference1),'')
	   --		ELSE
	   --		''
	   --END 		as vendor_name,

	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN
            ISNULL(jrnl_line.Reference2, ' ')
			ELSE
			''
	   END 		AS po_no,

	   --ISNULL(jrnl_line.Reference2, ' ')                   as po_no,

	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN
	        CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN
                 (SELECT invoice.apih_inv_num FROM [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.apinvoih AS invoice WHERE invoice.apih_vendor = jrnl_line.Reference1 AND invoice.apih_doc = jrnl_line.Reference3)
                ELSE
				 ISNULL(jrnl_line.Reference3, ' ')
            END
			ELSE
			''
	   END 		AS invoice_no,

	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN
	        CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN
                 (SELECT invoice_check.apih_check_no FROM [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.apinvoih AS invoice_check WHERE invoice_check.apih_vendor = jrnl_line.Reference1 AND invoice_check.apih_doc = jrnl_line.Reference3)
                ELSE
				 ' '
            END
			ELSE
			''
	   END 		AS check_no,

	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) IN ('API', 'POL', 'POE') THEN
	        CASE WHEN ISNUMERIC(jrnl_line.Reference1) = 1 AND ISNUMERIC(ISNULL(jrnl_line.Reference3, ' ')) = 1 THEN
                 (SELECT invoice_remit.apih_remit_no FROM [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.apinvoih AS invoice_remit WHERE invoice_remit.apih_vendor = jrnl_line.Reference1 AND invoice_remit.apih_doc = jrnl_line.Reference3)
                ELSE
				 ' '
            END
			ELSE
			  0
	   END AS remittance_no,

	   --ISNULL(jrnl_line.Reference3, ' ')                   as invoice_no,

	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') OR ISNUMERIC(jrnl_line.Reference1) = 0 THEN
            ISNULL(jrnl_line.Reference1, ' ')
			ELSE
			''
	   END 		AS reference_1,

	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') THEN
            ISNULL(jrnl_line.Reference2, ' ')
			ELSE
			''
	   END 		AS reference_2,

	   CASE WHEN LEFT(ISNULL(jrnl_line.Source, ' '),3) NOT IN ('API', 'POL', 'POE') THEN
            ISNULL(jrnl_line.Reference3, ' ')
			ELSE
			''
	   END 		AS reference_3,

       ISNULL(jrnl_line.Reference4, ' ')                   AS journal_description,
       ISNULL(jrnl_line.Source, ' ')                       AS journal_source,
       ISNULL(jrnl_line.TransactionType, ' ')              AS transaction_type,
       ISNULL(jrnl_line.BudgetType, ' ')                   AS budget_type,
       ISNULL(jrnl_line.Comment, ' ')                      AS journal_comment,
       CASE WHEN jrnl_line.OverBudget = 'True' THEN 'Y' ELSE 'N' END    AS over_budget,
       ISNULL(jrnl_line.AutoManual, ' ')                   AS auto_or_manual,
       --ISNULL(jrnl_line.DebitCredit, ' ')                  as debit_or_credit,
       jrnl.Year                                              AS transact_year,
       jrnl.Period                                            AS transact_period,
       jrnl_line.EffectiveDate  AS effective_date,
       jrnl.EntryDate           AS entry_date,

--       CONVERT(VARCHAR(10), jrnl_line.EffectiveDate, 101)  AS effective_date,
--       CONVERT(VARCHAR(10), jrnl.EntryDate, 101)              AS entry_date,

	   ISNULL(jrnl_line.Gross, 0.00)                        AS gross_amount,
	   ISNULL(jrnl_line.Debit, 0.00)                        AS debit_amount,
	   ISNULL(jrnl_line.Credit, 0.00)                       AS credit_amount,
	   ISNULL(Org.OrganizationCode, '')                     AS organization_code,
	   ISNULL(Proj.ProjectCode, '')                         AS project_code,
	   ISNULL(Proj.ProjectType, '')                         AS project_type
	   --OrgSeg1.*
	   --Seg1.*
       --ISNULL(jrnl_line.Entity, ' ')                       as a_entity_code,
       --ISNULL(dbo.Journals.AutoReverse, ' ')                          as h_auto_revers_y_n,
       --ISNULL(dbo.Journals.JournalType, ' ')                          as journal_type
       --CASE WHEN jrnl_line.PAApplied = 'True' THEN 'Y' ELSE 'N' END   as h_pa_applied,
       --CASE WHEN dbo.Journals.PostingClerk IS NULL OR dbo.Journals.PostingClerk = '' THEN ' ' ELSE dbo.Journals.PostingClerk END  as h_post_clerk,
       --Acct.Id                                                        as a_account_id
   FROM [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Journals AS jrnl
       INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.JournalLineItems AS jrnl_line ON jrnl_line.JournalId  = jrnl.Id
       INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Accounts AS Acct ON Acct.Id = jrnl_line.AccountId
       INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Organizations AS Org ON Acct.OrganizationId = Org.Id
       INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.GLObjects AS Obj ON Acct.ObjectId = Obj.Id
       LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Projects AS Proj ON Acct.ProjectId = Proj.Id
	   --
       LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.OrganizationSegments AS OrgSeg1
                 INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Segments AS Seg1 ON OrgSeg1.SegmentId = Seg1.Id AND Seg1.Type  = 1
				 ON Org.Id = OrgSeg1.OrganizationId
         LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.OrganizationSegments AS OrgSeg2

                   INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Segments AS Seg2 ON OrgSeg2.SegmentId = Seg2.Id AND Seg2.Type  = 2

           ON Org.Id = OrgSeg2.OrganizationId

         LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.OrganizationSegments AS OrgSeg3

                   INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Segments AS Seg3 ON OrgSeg3.SegmentId = Seg3.Id AND Seg3.Type  = 3

           ON Org.Id = OrgSeg3.OrganizationId

         LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.OrganizationSegments AS OrgSeg4

                   INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Segments AS Seg4 ON OrgSeg4.SegmentId = Seg4.Id AND Seg4.Type  = 4

           ON Org.Id = OrgSeg4.OrganizationId

         LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.OrganizationSegments AS OrgSeg5

                   INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Segments AS Seg5 ON OrgSeg5.SegmentId = Seg5.Id AND Seg5.Type  = 5

           ON Org.Id = OrgSeg5.OrganizationId

         LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.OrganizationSegments AS OrgSeg6

                   INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Segments AS Seg6 ON OrgSeg6.SegmentId = Seg6.Id AND Seg6.Type  = 6

           ON Org.Id = OrgSeg6.OrganizationId

         LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.OrganizationSegments AS OrgSeg7

                   INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Segments AS Seg7 ON OrgSeg7.SegmentId = Seg7.Id AND Seg7.Type  = 7

           ON Org.Id = OrgSeg7.OrganizationId

         LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.OrganizationSegments AS OrgSeg8

                   INNER JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.Segments AS Seg8 ON OrgSeg8.SegmentId = Seg8.Id AND Seg8.Type  = 8

           ON Org.Id = OrgSeg8.OrganizationId

         LEFT JOIN [YVWLNMUN10CS17.tylertech.com].mu0522.dbo.glcharcd_table AS cc ON cc.glch_code = Obj.CharacterCode

   -- CROSS JOIN sprviews ZZ
   --WHERE jrnl.Year >= 2012
   and jrnl.Phase IN ('History')  -- AND jrnl_line.Source = 'POL'

--   ZZ.sprv_idcode = CAST(substring(system_user,charindex('\',system_user)+1,len(system_user)) AS CHAR(20))
--     AND ZZ.sprv_gl     = 'Y'
--     AND dbo.Journals.Phase IN ('History')
